﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace Vanet
{
    class Test
    {
        public static Stopwatch _Timer1 = new Stopwatch();
        public static Random rnd = new Random();
        public static long _InitiaTime = 0;
        public static long _InitiaTimeAVG = 0;
        public static long _EncTime = 0;
        public static long _EncTimeAVG = 0;
        public static long _DecTime = 0;
        public static long _DecTimeAVG = 0;
        public static int _Bits = 32;
        public static int _NumRun = 0;
        public static int _n = 0;
        public static BigInteger _MM;
        public static BigInteger _X = 1;
        public static BigInteger _S = 0;
        public static ArrayList<BigInteger> _k = new ArrayList<BigInteger>();
        public static ArrayList<BigInteger> _m = new ArrayList<BigInteger>();
        public static ArrayList<BigInteger> _A_EEA = new ArrayList<BigInteger>();


        static void Main(string[] args)
        {
            RUN();
            Console.ReadLine();
        }

        private static void RUN()
        {
            _NumRun = 10;   // number of runs to make AVG
            _n = 10;  // number of receivers

            KeyGeneration();
            GenerateMessages();
            Initialization();
            for (int i = 0; i < _NumRun; i++)
            {
                System.println("Message:" + _m[i]);
                _C = Encryption(_m[i]);
                System.println("Cipher :" + _C);
                _MM = Decryption(_C,i);
                System.println("Decrypt:" + _MM);

                System.println(_m[i] != _MM ?"---------------------------------Error!!":"");
                System.println();

            }


            System.println("\n\n::The End:: \n\n");


        }




       
        // Returns modulo inverse of a with
        // respect to m using extended Euclid
        // Algorithm Assumption: a and m are
        // coprimes, i.e., gcd(a, m) = 1
        static BigInteger modInverse(BigInteger a, BigInteger m)
        {
            BigInteger m0 = m;
            BigInteger y = 0, x = 1;

            if (m == 1)
                return 0;

            while (a > 1)
            {
                // q is quotient
                BigInteger q = a / m;
                BigInteger t = m;

                // m is remainder now, process
                // same as Euclid's algo
                m = a % m;
                a = t;
                t = y;

                // Update x and y
                y = x - q * y;
                x = t;
            }

            // Make x positive
            if (x < 0)
                x += m0;

            return x;
        }


        private static BigInteger GCD_Test(BigInteger a, BigInteger b)
        {
            if (a == 0)
                return b;
            return GCD_Test(b % a, a);
        }



        private static BigInteger Decryption(BigInteger cipher, int i)
        {
           // System.println("\n\n::Decryption Method:: \n\n");

             _MM = 0;
            _Timer1.Reset();
            _Timer1.Start();

            _MM = (cipher % _k[i]);

            _Timer1.Stop();
            _DecTime += _Timer1.ElapsedTicks;

            return _MM;
           // System.println("M {0}        M_decrypt {1} ", _m[0], _MM);

            //Syste,.println("DecTime     {0} ", _DecTime);
        }

        public static BigInteger _C = 0;

        private static void GenerateMessages()
        {
       
            for(int i=0;i<_n;i++)
            {
                _m.Add(BigInteger.genPseudoPrime(_Bits, 10, rnd));
            }
            /*
            _m.Add(1524200607);
            _m.Add(1553968403);
            _m.Add(1575729728);
            _m.Add(1312199592);
            _m.Add(1043177704);
            _m.Add(1621880827);
            _m.Add(1852717674);
            _m.Add(1747646737);
            _m.Add(1692227643);
            _m.Add(1400258299);
            */
        
    }

    private static BigInteger Encryption(BigInteger message)
    {
            //  System.println("\n\n::Encryption Method:: \n\n");

            GenerateMessages();
            _C = 0;

            _Timer1.Reset();
            _Timer1.Start();

            _C = (message * _S);

            //_C %= _X;

            _Timer1.Stop();
            _EncTime += _Timer1.ElapsedTicks;

            return _C;
           // System.println("EncTime     {0} ", _EncTime);
        }

        private static void Initialization()
        {
           // System.println("\n\n::Initialization Method:: \n\n");

            _A_EEA = new ArrayList<BigInteger>();

            BigInteger _X_send = 1;
            _X = 1;

            _Timer1.Reset();
            _Timer1.Start();

            for (int i = 0; i < _n; i++)
            {
                _X *= _k[i];
            }

            for (int i = 0; i < _n; i++)
            {
                _X_send = _X / _k[i];
                _A_EEA.Add(modInverse(_X_send, _k[i]));
                _S += (_A_EEA[i] * _X_send);
            }

            _Timer1.Stop();
            _InitiaTime += _Timer1.ElapsedTicks;
           // System.println("InitiaTime     {0} ", _InitiaTime);
        }

        private static void KeyGeneration()
        {
            // System.println("\n\n::KeyGeneration Method:: \n\n");
            System.println("Keys :{");
            for (int i = 0; i < _n; i++)
            {
                _k.Add(BigInteger.genPseudoPrime(_Bits, 10, rnd));
                if (!_k[i].isProbablePrime())
                {
                    i--;
                    continue;
                }
                System.println("{0,-30}:{1}",_k[i],string.Format("{0,"+_Bits+"}",_k[i].ToString(2).Replace(" ","0")));

            }

            Console.Write("\b\b }\n");
            /*
            _k.Add(2103476027);
            _k.Add(1919776231);
            _k.Add(2116938521);
            _k.Add(1829224249);
            _k.Add(2052468853);
            _k.Add(2012814857);
            _k.Add(1982014319);
            _k.Add(1833900749);
            _k.Add(1733137229);
            _k.Add(2008622081);
            */

        }
    }
}
