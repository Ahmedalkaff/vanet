import java.math.BigInteger;

public class TestBigInteger {
    public static void main(String[] args) {
        BigInteger integer = new BigInteger("339601703568391538077");
        BigInteger integer1 = BigInteger.valueOf(241);
       // System.out.println(integer.modInverse(integer1));
        Original.modInverse(integer,integer1);

        BigInteger root = integer.sqrt();
        for(BigInteger i = BigInteger.TWO;i.compareTo(root) != 0;i = i.add(BigInteger.ONE))
        {
           // System.out.println("i = " + i);

            if((integer.remainder(i)).compareTo(BigInteger.ZERO) == 0)
            {
                System.out.println("Not prime on :"+i);
                break;
            }
        }

        System.out.println("integer = " + integer +" PrimeTest:"+integer.isProbablePrime(10));

        System.out.println("integer1 = " + integer1 +" PrimeTest:"+integer1.isProbablePrime(10));
//        System.out.println(integer.modInverse(integer1));
//        System.out.println();
//        BigInteger big = BigInteger.ONE;
//        int i = 1;
//        while (true) {
//            try {
//                big = big.multiply(BigInteger.valueOf(Integer.MAX_VALUE));
//                i++;
//                System.out.println("big = " + big.bitCount());
////                if(i > 10)
////                    break;
//            } catch (Exception ex) {
//                System.out.println("Stop i:" + i);
//                System.out.println("Big:" + big);
//
//                break;
//            }

//        }
    }
}
