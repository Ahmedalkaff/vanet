import java.math.BigInteger;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class CRT {

    private static long mStartTime, mEndTime;

    private  static final Random rnd = new Random();
    public static int NumberOfBits = 64;
    public static int NumberOfRuns = 0;
    public static int NumberOfReceivers = 0;
    public static BigInteger _X = BigInteger.ONE;
    public static BigInteger _S = BigInteger.ZERO;
    public static BigInteger cipher = BigInteger.ZERO;
    public static ArrayList<BigInteger> _k = new ArrayList<>();
    public static ArrayList<BigInteger> messages = new ArrayList<>();
    public static ArrayList<BigInteger> _A_EEA = new ArrayList<>();

    public static void main(String[] args) {
       RUN();

    }


    private static void RUN() {
        NumberOfRuns = 1;   // number of runs to make AVG
        NumberOfReceivers = 10;  // number of receivers

        KeyGeneration();
        generateMessages();
        for (int i = 0; i < NumberOfRuns; i++) {
            Initialization();
            Encryption();
            Decryption();
        }

        System.out.println("\n\n::The End:: \n\n");

    }


    private static void Decryption() {
        System.out.println("\n\n::Decryption Method:: \n\n");
        BigInteger _MM = BigInteger.ZERO;
        mStartTime = System.currentTimeMillis();
        _MM = cipher.mod(_k.get(0));

        mEndTime = System.currentTimeMillis();

        System.out.printf(Locale.getDefault(), "M %s        M_decrypt %s ", messages.get(0), _MM);

        System.out.printf(Locale.getDefault(), "decryptionTime     %d ms %n", (mEndTime - mStartTime));
    }

    private static void DecryptionText() {
        System.out.println("\n\n::Decryption Method:: \n\n");
        BigInteger _MM = BigInteger.ZERO;
        mStartTime = System.currentTimeMillis();
        _MM = cipher.mod(_k.get(5));

        mEndTime = System.currentTimeMillis();

        System.out.printf(Locale.getDefault(), "M %s        M_decrypt %s ", messages.get(0), _MM);

        System.out.printf(Locale.getDefault(), "decryptionTime     %d ms %n", (mEndTime - mStartTime));
    }


    private static void Encryption(String plaintext) {
        System.out.println("\n\n::Encryption Method:: \n\n");

        for(int i=0;i<NumberOfReceivers;i++)
            messages.add(BigInteger.probablePrime(NumberOfBits,rnd));

        cipher = BigInteger.ZERO;

        mStartTime = System.currentTimeMillis();
        cipher = messages.get(0).multiply(_S);
        cipher = cipher.mod(_X);

        mEndTime = System.currentTimeMillis();

        System.out.printf(Locale.getDefault(),"EncTime     %d ms %n", (mEndTime - mStartTime));
    }

    private  static ArrayList<BigInteger> generateMessages(){
        for (int i = 0; i < NumberOfReceivers; i++) {
            messages.add(BigInteger.probablePrime(NumberOfBits,rnd));
        }

        return messages;
    }
    private static void Encryption() {
        System.out.println("\n\n::Encryption Method:: \n\n");



        cipher = BigInteger.ZERO;

        mStartTime = System.currentTimeMillis();
        cipher = messages.get(0).multiply(_S);
        cipher = cipher.mod(_X);

        mEndTime = System.currentTimeMillis();

        System.out.printf(Locale.getDefault(),"EncTime     %d ms %n", (mEndTime - mStartTime));
    }

    private static void Initialization() {
//        System.out.println("\n\n::Initialization Method:: \n\n");

        _A_EEA = new ArrayList<>();

        BigInteger _X_send = BigInteger.ONE;
        _X = BigInteger.ONE;

        mStartTime = System.currentTimeMillis();

        for (int i = 0; i < NumberOfReceivers; i++) {
            _X = _X.multiply(_k.get(i));
        }

        for (int i = 0; i < NumberOfReceivers; i++) {
            _X_send = _X.divide(_k.get(i));
            _A_EEA.add(_X_send.mod(_k.get(i)));
            _S = _S.add(_A_EEA.get(i).multiply(_X_send));
        }

        mEndTime = System.currentTimeMillis();

//        System.out.printf(Locale.getDefault(), "startTime     %d ms %n", (mEndTime - mStartTime));
    }

    private static ArrayList<BigInteger> KeyGeneration() {
        System.out.println("\n\n::KeyGeneration Method:: \n\n");

        for(int i=0;i<NumberOfReceivers;i++)
            _k.add(BigInteger.probablePrime(NumberOfBits,rnd));

        return _k ;

    }
}
