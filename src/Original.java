import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;

public class Original {

    private static PrintWriter writer = null;

    static {
        try {
            writer = new PrintWriter(new FileWriter(new File("Original.txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int NumberBits = 8;
    private static final int d = NumberBits + 2;
    private static final int nRuns = 10;   // number of runs to make AVG
    private static final int nReceivers = 10;  // number of receivers
    private static int[] failureCounters = new int[nRuns];  // number of receivers
    public static long initialTime = 0;
    public static long encryptionTime = 0;
    public static long decryptionTime = 0;
    public static long messageGenerationTime = 0;
    private static long keyGenerationTime = 0;

    public static long startTime = 0;

    private static final Random rnd = new Random();

    private static BigInteger DecryptedMessage;
    public static BigInteger _X = BigInteger.ONE;
    public static BigInteger _S = BigInteger.ZERO;
    public static BigInteger _X_send;
    public static ArrayList<BigInteger> keyArrayList = new ArrayList<>();
    public static ArrayList<BigInteger> messageArrayList = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _A_EEA = new ArrayList<BigInteger>();


    public static void main(String[] args) {

        boolean result;
        writer.println("Round ,Number of bits , result , key, message ,cipher , decrypted message, Key generation time , encryption time , decryption time ");

        while (NumberBits <= 2048) {
            geyGeneration();
            //  testKeys();
            GenerateMessage();
//            for (int i = 0; i < K.size(); i++) {
//                if (!K.get(i).isProbablePrime(10) ) {
//                    System.out.printf("Key:%30d :%s%n", K.get(i), K.get(i).isProbablePrime(10));
//                }
//            }
            Initialization();
//            writer.printf("%5s,",NumberBits);
            //writer.println("Keys :"+K);
//            writer.printf(" %-20s:%20d(ns),%-20s:%20d (ns),%-20s:%20d(ns)%n",
//                    "KeyGen time", keyGenerationTime,
//                    "Msgs Gen time", messageGenerationTime,
//                    "Initial time", initialTime);
            for (int i = 0; i < nRuns; i++) {

                cipher = Encryption(messageArrayList.get(i));
                DecryptedMessage = Decryption(cipher, i);
                result = DecryptedMessage.compareTo(messageArrayList.get(i)) == 0;
                failureCounters[i] += result ? 1 : 0;

                writer.printf(Locale.getDefault(),
                        "%-5d, %-5d, %-5s, %s, %s, %s, %d, %d, %d, %d%n",
                        i, NumberBits, (result ? "Pass" : "Failed"), timeToString(keyGenerationTime), timeToString(encryptionTime), timeToString(decryptionTime),
                        keyArrayList.get(i), messageArrayList.get(i), cipher, DecryptedMessage);
            }
            NumberBits <<= 1;
        }


        System.out.println("Done");
//        for(int i=0;i<failureCounters.length;i++)
//        {
//            System.out.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],nRuns);
//            writer.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],nRuns);
//        }
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void testKeys() {
        if (new HashSet<BigInteger>(keyArrayList).size() == keyArrayList.size())
            System.out.println("No redundant keys");
        else
            System.err.println("Some keys are redundant");
        BigInteger x = BigInteger.ONE;
        for (BigInteger k : keyArrayList)
            x = x.multiply(k);

//        System.out.println("Keys:"+K);
//        System.out.println("x = " + x);
        BigInteger s = BigInteger.ONE;
        for (BigInteger k : keyArrayList) {
            System.out.print(((x.divide(k)).gcd(k)) + ", ");
        }
        System.out.println();
    }


    private static BigInteger Decryption(BigInteger cipher, int i) {
//        writer.println("\n\n::Decryption Method:: \n\n");

        DecryptedMessage = BigInteger.ZERO;

        long startTime = System.nanoTime();
        DecryptedMessage = cipher.remainder(keyArrayList.get(i));

        //DecryptedMessage = cipher.mod(K.get(i));
        decryptionTime = System.nanoTime() - startTime;

        return DecryptedMessage;
    }

    public static BigInteger cipher = BigInteger.ZERO;


    private static void GenerateMessage() {
        messageArrayList.clear();
        for (int i = 0; i < nReceivers; i++)
            messageArrayList.add(new BigInteger(NumberBits, rnd));
//        BigInteger prime;
//        startTime = System.nanoTime();
//        for (int i = 0; i < nReceivers; ) {
//            prime = BigInteger.probablePrime(NumberBits, rnd);
//            if (prime.isProbablePrime(10) ) {
//                messageArrayList.add(prime);
//                i++;
//            }
//
//        }

        messageGenerationTime = System.nanoTime() - startTime;
    }

    private static String timeToString(long time) {

        if (time < 1000) return String.format(Locale.getDefault(), "%8d (ns)", time);
        if (time < 1000000) return String.format(Locale.getDefault(), "%8.2f (µs)", (time / 1000.0));
        if (time < 10000000000L) return String.format(Locale.getDefault(), "%8.2f (ms)", (time / 1000000.0));
        return String.format(Locale.getDefault(), "%8.2f (s)", (time / 1000000000.0));
    }

    private static BigInteger Encryption(BigInteger message) {
//        writer.println("\n\n::Encryption Method:: \n\n");


        //GenerateMessage();
        cipher = BigInteger.ZERO;

       long startTime = System.nanoTime();

        cipher = message.multiply(_S);
        //cipher = cipher.mod(M);

        encryptionTime = System.nanoTime() - startTime;

//        writer.printf(Locale.getDefault(), "EncTime     %d %n", (EndTime - initialTime));

        return cipher;
    }

    private static void Initialization() {
//        writer.println("\n\n::Initialization Method:: \n\n");

        if (_A_EEA == null)
            _A_EEA = new ArrayList<>();
        else
            _A_EEA.clear();

        startTime = System.nanoTime();


        _S = BigInteger.ZERO;
        _X_send = BigInteger.ONE;
        _X = BigInteger.ONE;

        for (int i = 0; i < nReceivers; i++) {
            _X = _X.multiply(keyArrayList.get(i));
//            writer.println(M);
        }

//        writer.println("M :" + M);
        int c = 0;
        for (int i = 0; i < nReceivers; i++) {
            try {
                _X_send = _X.divide(keyArrayList.get(i));
                c = 1;
                _A_EEA.add(_X_send.modInverse(keyArrayList.get(i)));
                //MmodInvers.add(modInverse(Mi, K.get(i)));
                c = 2;
                _S = _S.add(_A_EEA.get(i).multiply(_X_send));
            } catch (Exception e) {
                writer.printf("Round [%d]--------------------------%n", i);
                writer.println(e.getMessage());
                writer.println("Mi = " + _X_send);
                writer.println("K = " + keyArrayList);
                writer.println("K.get(i) = " + keyArrayList.get(i));
                writer.println("MmodInvers = " + _A_EEA);
                writer.println("X = " + _S);
                writer.println("--------------------------");
                return;

            }
        }

        initialTime = System.nanoTime() - startTime;
//        writer.printf(Locale.getDefault(), "startTime     %s %n", (EndTime - initialTime));
    }

    public static BigInteger modInverse(BigInteger a, BigInteger m) {

        BigInteger m0 = m;
        BigInteger y = BigInteger.ZERO, x = BigInteger.ONE;

        if (m.compareTo(BigInteger.ONE) == 0)
            return BigInteger.ZERO;

        while (a.compareTo(BigInteger.ONE) > 0) {
            // q is quotient
            BigInteger q = a.divide(m);
            BigInteger t = m;

            // m is remainder now, process
            // same as Euclid's algo
            m = a.remainder(m);
            a = t;
            t = y;

            // Update x and y
            y = x.subtract(q.multiply(y));
            x = t;
        }

        // Make x positive
        if (x.compareTo(BigInteger.ZERO) < 0)
            x = x.add(m0);


        return x;
    }

    private static void geyGeneration() {
//        writer.println("\n\n::geyGeneration Method:: \n\n");


        BigInteger prime;
        keyArrayList.clear();
        startTime = System.nanoTime();
        for (int i = 0; i < nReceivers; ) {
            prime = BigInteger.probablePrime(NumberBits + 1, rnd);
            if (prime.isProbablePrime(10) && !keyArrayList.contains(prime)) {
                keyArrayList.add(prime);
                i++;
            }

//            BigInteger root = K.get(i).sqrt();
//            for(BigInteger j =BigInteger.TWO; j.compareTo(root) < 0; j = j.add(BigInteger.ONE) )
//                if((K.get(i).mod(j)).compareTo(BigInteger.ZERO) == 0)
//                {
//                    writer.println(K.get(i)+ " is not a prime");
//                    i--;
//                    break;
//                }
////            if (!K.get(i).isProbablePrime(10))
////                i--;


            keyGenerationTime = System.nanoTime() - startTime;
        }


//        K.add(BigInteger.valueOf(2103476027));
//        K.add(BigInteger.valueOf(1919776231));
//        K.add(BigInteger.valueOf(2116938521));
//        K.add(BigInteger.valueOf(1829224249));
//        K.add(BigInteger.valueOf(2052468853));
//        K.add(BigInteger.valueOf(2012814857));
//        K.add(BigInteger.valueOf(1982014319));
//        K.add(BigInteger.valueOf(1833900749));
//        K.add(BigInteger.valueOf(1733137229));
//        K.add(BigInteger.valueOf(2008622081));
//
//        writer.println("K = " + K);

    }


}