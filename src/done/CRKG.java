package done;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;
import static helper.TimeHelper.timeToString ;

/**
 *  K1 2007 FROM K12 Chinese Remainder Theorem Based Group Key , CRKG (first algorithm)
 *
 *  fully implemented and tested
 */
public class CRKG {

    private static final int UsersFactor = 2;
    private static PrintWriter writer = null;
    private static long keyPoolGenerationTime;
    private static long memberJoinTime;
    private static long memberLeaveningTime;
    private static long MassLeavetime;
    private static long InitializationTime = 0;
    private static long ClientsKeyGetTime = 0;
    private static String  filename;

    static {
        try {
             filename = new Object(){}.getClass().getEnclosingClass().getSimpleName();
             File file = new File(filename+".txt");
            writer = new PrintWriter(new FileWriter(file,true));

            if(!file.exists())
                writer.printf(Locale.getDefault(), "Algorithm,Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final int MAX_KEY_LENGTH = 1024;
    private static int NumberBits = 32;
    private static final int d = NumberBits + 2;
    private static final int RUNS = 10;   // number of runs to make AVG
    private static final int INITITAL_USERS = 10;  // number of receivers

    private static final Random rnd = new Random();

    private static BigInteger M = BigInteger.ONE;
    private static BigInteger X = BigInteger.ZERO;



    static int [] usersToAdd = {1,3,7,13,23,33};
    public static void main(String[] args) {

        boolean result;
        ArrayList<Integer> toRemove = new ArrayList<>();

        while (NumberBits <= MAX_KEY_LENGTH) {
            keyPoolGenerationTime = 0;
            InitializationTime = 0;
            ClientsKeyGetTime = 0;


            for(int i=0;i<RUNS;i++) {
                keyPoolGeneration(INITITAL_USERS * UsersFactor, false);
                groupInitialization(INITITAL_USERS);
                clean();
                System.out.println("Run :"+(i+1));
            }
            writer.printf(Locale.getDefault(), "%s,%d,%d,%d,%s,%s,%s%n",
                    filename, RUNS,NumberBits, INITITAL_USERS, timeToString(keyPoolGenerationTime/RUNS), timeToString(InitializationTime/RUNS), timeToString(ClientsKeyGetTime / INITITAL_USERS*RUNS));


            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            NumberBits <<= 1;
        }
        writer.println();
        writer.close();

        System.out.println("Done");
//        for(int i=0;i<failureCounters.length;i++)
//        {
//            System.out.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//            writer.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//        }
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void clean() {

        System.gc();
        KeyPool.clear();
        UsersKeys.clear();
        Users.clear();
        ServerGroupKey=BigInteger.ZERO;
        X=BigInteger.ZERO;
    }

    private static void test() {
        String result = "";
        for (User u : Users) {
            if (ServerGroupKey.compareTo(u.GroupKey) == 0)
                result = "PASSED";
            else
                result = "FAILED!!";
            System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, result, "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
            System.out.println("--------------------------------");
        }
    }

    private static ArrayList<BigInteger> KeyPool = new ArrayList<>();;
    private static ArrayList<BigInteger> UsersKeys = new ArrayList<>();
    private static LinkedList<User> Users = new LinkedList<>();

    private static void keyPoolGeneration(int poolSize, boolean extend) {
        BigInteger prime;

        if (KeyPool == null || !extend)
            KeyPool = new ArrayList<>(poolSize);
        BigInteger K = BigInteger.ZERO;
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(NumberBits, rnd);
            if (prime.isProbablePrime(10) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }
        keyPoolGenerationTime += System.nanoTime() - startTime;
    }

    private static BigInteger ServerGroupKey = BigInteger.TEN;

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static void groupInitialization(int numberOfUsers) {
      //  ServerGroupKey = generateGroupKey();
        M = BigInteger.ONE;
        BigInteger temp;
//        if (numberOfUsers > KeyPool.size())
//            keyPoolGeneration(numberOfUsers * UsersFactor, true);
        long startTime = System.nanoTime();
        do {
            temp = KeyPool.get(rnd.nextInt(KeyPool.size()));
            Users.add(new User(temp));
            UsersKeys.add(temp);
            M = M.multiply(temp);
            removeKeyFromPool(temp);

        } while (Users.size() < numberOfUsers);
        X = computeX();
        broadcastX(X);
        InitializationTime += System.nanoTime() - startTime;
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static BigInteger generateGroupKey() {
        return BigInteger.probablePrime(NumberBits, rnd);
    }

    static BigInteger Ki = BigInteger.TEN, Mi = BigInteger.TEN, MiInv = BigInteger.TEN;

    private static BigInteger computeX() {
        X = BigInteger.ZERO;
        ServerGroupKey = generateGroupKey();
        for (User u : Users) {
            Ki = BigInteger.TEN;
            Mi = BigInteger.TEN;
            MiInv = BigInteger.TEN;
            try {
                Ki = ServerGroupKey.xor(u.PrivateKey);
                Mi = M.divide(u.PrivateKey);
                MiInv = Mi.modInverse(u.PrivateKey);
                X = X.add((Ki.multiply(Mi.multiply(MiInv)))).mod(M);

            } catch (Exception ex) {
                System.out.print("User ID = " + u.ID + "\t");
                System.out.print("ServerGroupKey = " + ServerGroupKey + "\t");
                System.out.print("Ui = " + u.PrivateKey + "\t");
                System.out.print("Ki = " + Ki + "\t");
                System.out.print("Mi = " + Mi + "\t");
                System.out.print("MiInv = " + MiInv + "\t");
                System.out.println("X = " + X);
            }
        }
        return X.mod(M);
    }


    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static void broadcastX(BigInteger X) {
        for (User u : Users)
            u.computeGroupKey(X);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static void memberJoin(int n) {
        long startTime = System.nanoTime();
        if (n >= KeyPool.size())
            keyPoolGeneration((KeyPool.size()+n) * UsersFactor, true);
        BigInteger Unew;
        for (int i = 0; i < n; i++) {
            Unew = KeyPool.get(rnd.nextInt(KeyPool.size()));
            Users.add(new User(Unew));
            UsersKeys.add(Unew);
            M=M.multiply(Unew);
            removeKeyFromPool(Unew);
        }
        X = computeX();
        broadcastX(X);

        memberJoinTime = System.nanoTime() - startTime;
    }

    private static LinkedList<User> RemovedUsers = new LinkedList<>();

    private static void memberLeave(int Userid) {
        if (Userid < 0 || Userid >= UsersKeys.size())
            throw new IllegalArgumentException();

        long startTime = System.nanoTime();
        BigInteger removed = UsersKeys.remove(Userid);
        RemovedUsers.add(Users.remove(Userid));

        removeKeyFromPool(removed);
        X = computeX();
        broadcastX(X);
        memberLeaveningTime = System.nanoTime() - startTime;

    }

    private static void memberLeave(ArrayList<Integer> Userids) {
        if (Userids == null || Userids.size() < 1)
            throw new IllegalArgumentException();

        // sort the users Ids decadently to grantee
        // that removing an element will not effect the id of the next id to be removed
        Userids.sort((a,b)->b.compareTo(a));
        long startTime = System.nanoTime();
        BigInteger removed;
     //  int shift =0,index ;
       try {
           for (int id : Userids) {
               removed = UsersKeys.remove(id );
               RemovedUsers.add(Users.remove(id));
               removeKeyFromPool(removed);
           }
       }catch (Exception ex)
       {
           ex.printStackTrace();
       }

        X = computeX();
        broadcastX(X);
        MassLeavetime = System.nanoTime() - startTime;
        

    }

    private static void removeKeyFromPool(BigInteger removed) {
        BigInteger temp;
        do {
            temp = BigInteger.probablePrime(NumberBits, rnd);
        } while (KeyPool.contains(temp));
        KeyPool.add(temp);
        KeyPool.remove(removed);

    }

    static class User {
        private static int _id = 1;
        private int ID;
        private BigInteger GroupKey, PrivateKey;

        public User(BigInteger Uk) {
            ID = _id++;
            setPrivateKey(Uk);
        }

        public BigInteger getGroupKey() {
            return GroupKey;
        }

        public void setPrivateKey(BigInteger privateKey) {
            PrivateKey = privateKey;
        }

        private void computeGroupKey(BigInteger X) {
            long startTime = System.nanoTime();
            GroupKey = X.mod(PrivateKey).xor(PrivateKey);
            ClientsKeyGetTime += System.nanoTime() - startTime;
        }
    }
}