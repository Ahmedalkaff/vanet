package done;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;

import static helper.TimeHelper.timeToString;

/**
 * K12 2016 Dual Authentication and Key Management Techniques
 */
public class VGKM {

    private static final int FACTOR = 3;
    private static PrintWriter writer = null;
    private static ArrayList<BigInteger> KeyPool;
    private static BigInteger ServerGroupKey;
    private static BigInteger Ypug;
    static BigInteger P, Q;
    public static BigInteger M = BigInteger.ONE;
    public static BigInteger X = BigInteger.ZERO;
    static BigInteger Mu = BigInteger.ONE;
    static ArrayList<BigInteger> Xi = new ArrayList<>();
    static ArrayList<BigInteger> Yi = new ArrayList<>();
    static ArrayList<BigInteger> VARi = new ArrayList<>();


    static {
                try {
                    String filename = new Object(){}.getClass().getEnclosingClass().getSimpleName();
                    writer = new PrintWriter(new FileWriter(new File(filename+".txt")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
    }

    private static final int MAX_KEY_LENGTH = 1024;
    private static int NumberBits = 32;
    private static final int d = NumberBits + 2;
    private static final int nRuns = 10;   // number of runs to make AVG
    private static final int INITITAL_USERS = 10;  // number of receivers
    private static int[] failureCounters = new int[nRuns];  // number of receivers
    public static long InitializationTime = 0;
    private static long keyPoolGenerationTime;
    private static long ClientsKeyGetTime;


    private static final Random rnd = new Random();




    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    public static void main(String[] args) {

        writer.printf(Locale.getDefault(), "Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");

        while (NumberBits <= MAX_KEY_LENGTH) {

            keyPoolGeneration(INITITAL_USERS * FACTOR,NumberBits);
//            System.out.println("KeyPool = " + KeyPool);
            initialSetUp(INITITAL_USERS,NumberBits);

            writer.printf(Locale.getDefault(), "%d,%d,%s,%s,%s%n",
                    NumberBits, INITITAL_USERS, timeToString(keyPoolGenerationTime), timeToString(InitializationTime),timeToString(ClientsKeyGetTime/INITITAL_USERS));
           // test();
            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            clean();
            NumberBits <<= 1;


        }
        writer.close();


        System.out.println("Done");
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void clean() {
        keyPoolGenerationTime = 0;

        InitializationTime = 0;
        ClientsKeyGetTime = 0;
        System.gc();
        Xi.clear();
        Yi.clear();;
        VARi.clear();
        Mu=BigInteger.ZERO;
        KeyPool.clear();
        M= BigInteger.ONE;
        X = BigInteger.ZERO;
        PUSKs.clear();
        SUSKs.clear();
        Users.clear();
        ServerGroupKey=BigInteger.ZERO;
        X=BigInteger.ZERO;
    }

    private static void test() {
        String result = "";
        for (User u : Users) {
            if (ServerGroupKey.compareTo(u.GroupKey) == 0)
                result = "PASSED";
            else
                result = "FAILED!!";
            System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, result, "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
            System.out.println("--------------------------------");
        }
    }


    private static void initialSetUp(int n, int numberBits) {

        long start = System.nanoTime();
        do {
            P = BigInteger.probablePrime(numberBits, rnd);
            Q = BigInteger.probablePrime(numberBits - 2, rnd);
        } while (!P.isProbablePrime(10) ||! Q.isProbablePrime(10) || !( Q.compareTo(P.shiftLeft(2)) < 0));

        userInitialisation(n);

        for (int i = 0; i < PUSKs.size(); i++) {
            Xi.add(M.divide(PUSKs.get(i)));
            Yi.add(Xi.get(i).modInverse(PUSKs.get(i)));
            VARi.add(Xi.get(i).multiply(Yi.get(i)));
            Mu = Mu.add(VARi.get(i));
        }
        groupKeyComputation();
        InitializationTime =  System.nanoTime() - start;

    }

    private static void keyPoolGeneration(int poolSize, int bit) {
        BigInteger prime;

        if (KeyPool == null)
            KeyPool = new ArrayList<>(poolSize);
        BigInteger K = BigInteger.ZERO;
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(bit, rnd);
            if (prime.isProbablePrime(10) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }
        keyPoolGenerationTime = System.nanoTime() - startTime;
    }


    private static LinkedList<User> Users = new LinkedList<>();
    private static LinkedList<BigInteger> PUSKs = new LinkedList<>();
    private static LinkedList<BigInteger> SUSKs = new LinkedList<>();


    private static void userInitialisation(int n) {
        M = BigInteger.ONE;
        while (Users.size() < n) {
            PUSKs.add(KeyPool.remove(0));
            M = M.multiply(PUSKs.getLast());
            SUSKs.add(KeyPool.remove(0));
            Users.add(new User(PUSKs.getLast(), SUSKs.getLast()));
        }

    }


    private static void groupKeyComputation() {
        // from
        //
        ServerGroupKey = generateGroupKey(Q);
        Ypug = ServerGroupKey.multiply(Mu);

        // to
        broadCast(Ypug,Users);

    }

    private static void broadCast(BigInteger ypug, LinkedList<User> users) {
        for(User u : users)
        {
            u.computeGroupKey(ypug);
        }
    }


    private static BigInteger generateGroupKey(BigInteger q) {
        BigInteger t = null;
        do {
            t = new BigInteger(rnd.nextInt(q.bitCount()), rnd);
            if (t.compareTo(q) < 0)
                return t;
        } while (true);
    }


    static class User {
        private static int _id = 1;
        private int ID;
        private BigInteger SUSK, PUSK;

        private BigInteger GroupKey;


        public User(BigInteger pusk, BigInteger susk) {
            ID = _id++;
            SUSK = susk;
            PUSK = pusk;
        }

        public BigInteger getGroupKey() {
            return ServerGroupKey;
        }

        public void setPrivateKey(BigInteger privateKey) {
//            PrivateKey = privateKey;
        }

        private void computeGroupKey(BigInteger X) {
            long startTime = System.nanoTime();
            GroupKey = X.mod(PUSK);
            ClientsKeyGetTime += System.nanoTime() - startTime;

        }

    }
}