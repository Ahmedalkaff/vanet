package done;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;

import static helper.TimeHelper.timeToString;

/**
 * K1 2007 FROM K12 Chinese Remainder Theorem Based Group Key , FCRKG (second algorithm)
 * <p>
 * member join and remove need to be tested if the key pool size is extended
 */
public class FCRKG {


    // TODO: calc

    private static final int Factor = 3;
    private static long MassLeavetime;
    private static PrintWriter writer = null;
    private static long keyPoolGenerationTime;
    private static long InitializationTime;
    private static long ClientsKeyGetTime;




    static {
        try {
            String filename = new Object() {
            }.getClass().getEnclosingClass().getSimpleName();
            writer = new PrintWriter(new FileWriter(new File(filename + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final int MAX_KEY_LENGTH = 1024;
    private static int NumberBits = 32;
    private static final int d = 3;
    private static final int RUNS = 3;   // number of runs to make AVG
    private static final int INITITAL_USERS = 10;  // number of receivers
    private static int[] failureCounters = new int[RUNS];  // number of receivers
    public static long initialTime = 0;
    public static long encryptionTime = 0;
    public static long decryptionTime = 0;
    public static long messageGenerationTime = 0;
    private static long keyGenerationTime = 0;

    public static long startTime = 0;

    private static final Random rnd = new Random();

    private static BigInteger DecryptedMessage;
    public static BigInteger M = BigInteger.ONE;
    public static BigInteger X = BigInteger.ZERO;

    public static ArrayList<BigInteger> KeyArrayList = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> messageArrayList = new ArrayList<BigInteger>();


    static int[] usersToAdd = {1, 3, 7, 13, 23, 33};

    public static void main(String[] args) {

        boolean result;
        ArrayList<Integer> toRemove = new ArrayList<>();
        writer.printf(Locale.getDefault(), "Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");

        while (NumberBits <= MAX_KEY_LENGTH) {

            int m = INITITAL_USERS * Factor;
            // keyPoolGeneration(m* Factor, false);
            groupInitialization(INITITAL_USERS, m);
            writer.printf(Locale.getDefault(), "%d,%d,%s,%s,%s%n",
                    NumberBits, INITITAL_USERS, timeToString(keyPoolGenerationTime), timeToString(InitializationTime), timeToString(ClientsKeyGetTime / INITITAL_USERS));

//            int temp ;
//            toRemove.clear();
////            memberJoin(10);
//            for(int u : usersToAdd)
//            {
//                System.out.printf(Locale.getDefault(), "Current users %d , keys :%d, last users:%d , Last keys:%d adding %d users, KeyPool:%d, KeyPoolIndex:%d%n",Users.size(),UsersKeys.size(),lastUsers,lastKeys,  u,KeyPool.size(),KeyPoolIndex);
//
//                    memberJoin(1);
//                    test();
//                    writer.printf(Locale.getDefault(),"Time to add (%d) user(s) is :%s%n",u, timeToString(memberJointime));
////                    while(toRemove.size()<u)
////                    {
////                        temp = rnd.nextInt(Users.size());
////                        if(!toRemove.contains(temp))
////                            toRemove.add(temp);
////                    }
////                    memberLeave(toRemove);
////                    writer.printf(Locale.getDefault(),"Time to remove (%d) user(s) is :%s%n",u, timeToString(MassLeavetime));
//
//            }
//               test();
            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            clean();
            NumberBits <<= 1;


        }
        writer.close();


        System.out.println("Done");
//        for(int i=0;i<failureCounters.length;i++)
//        {
//            System.out.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//            writer.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//        }
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void clean() {


        System.gc();

        X = BigInteger.ZERO;
        M = BigInteger.ONE;

        keyPoolGenerationTime = 0;

        InitializationTime = 0;
        ClientsKeyGetTime = 0;
        lastUsers = 0;
        lastKeys = 0;
        KeyPoolIndex = 0;

        KeyPool.clear();
        UsersKeys.clear();
        Users.clear();
        ServerGroupKey = BigInteger.ZERO;
    }

    private static void test() {
        for (User u : Users) {
            if (ServerGroupKey.compareTo(u.GroupKey) == 0) {
                System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n", u.ID, NumberBits, "PASSED");
                System.out.println("--------------------------------");
            } else {
                System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, "FAILED", "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
                System.out.println("--------------------------------");
            }


        }
    }

    private static ArrayList<BigInteger> KeyPool;
    private static ArrayList<BigInteger> UsersKeys = new ArrayList<>();
    private static ArrayList<User> Users = new ArrayList<>();

    private static int KeyPoolIndex = 0;

    private static void keyPoolGeneration(int poolSize, boolean extend) {
        BigInteger prime;

        if (KeyPool == null || !extend)
            KeyPool = new ArrayList<>(poolSize);
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(NumberBits, rnd);
            if (prime.isProbablePrime(10) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }

        keyPoolGenerationTime = System.nanoTime() - startTime;
//        System.out.println("KeyPool = " + KeyPool);
    }

    private static BigInteger ServerGroupKey = BigInteger.TEN;

    private static void groupInitialization(int users, int keys) {
        //  ServerGroupKey = generateGroupKey();
        if (users <= lastUsers || keys <= lastKeys || users > keys)
            throw new ArithmeticException();

        //M = BigInteger.ONE;
        BigInteger temp;
        long startTime = System.nanoTime();

        if (KeyPool == null || keys > KeyPool.size())
            keyPoolGeneration(keys * Factor, true);

        for (int i = Users.size(); i < keys; i++) {
            try {
                if (i >= lastKeys) {
                    temp = KeyPool.get(KeyPoolIndex++);
                    UsersKeys.add(i, temp);
                    M = M.multiply(temp);
                }

                if (i < users) {
                    Users.add(new User(UsersKeys.get(i)));
                }

            } catch (Exception ex) {
                System.out.println("ex.getMessage() = " + ex.getMessage());
            }

        }

//        lastKeys = UsersKeys.size();
//        lastUsers = Users.size() ;

//        System.out.println("Users = " + Users);
//        System.out.println("UsersKeys = " + UsersKeys);
        X = computeX();
//        System.out.println("X = " + X);
        broadcastX(X);
//        System.out.println("ServerGroupKey = " + ServerGroupKey);
//        System.out.println("Users after x = " + Users);

        InitializationTime = System.nanoTime() - startTime;
    }

    private static BigInteger generateGroupKey() {
        return BigInteger.probablePrime(NumberBits, rnd);
    }

    static ArrayList<BigInteger> Ki = new ArrayList<>();
    static ArrayList<BigInteger> Mi = new ArrayList<>();
    static ArrayList<BigInteger> MiInv = new ArrayList<>();

    static int lastUsers = 0, lastKeys = 0;

    private static BigInteger computeX() {
        if (X.compareTo(BigInteger.ZERO) == 0) {
            ServerGroupKey = generateGroupKey();
            for (int i = 0; i < UsersKeys.size(); i++) {
                if (i < Users.size())
                    Ki.add(i, ServerGroupKey.xor(Users.get(i).PrivateKey));
                else
                    Ki.add(i, BigInteger.probablePrime(NumberBits, rnd));

                Mi.add(i, M.divide(UsersKeys.get(i)));
                MiInv.add(i, Mi.get(i).modInverse(UsersKeys.get(i)));
                X = X.add(Ki.get(i).multiply(Mi.get(i)).multiply(MiInv.get(i)));
            }

        } else {
            for (int i = lastUsers; i < UsersKeys.size(); i++) {
                try {
                    if (i >= lastKeys) {
                        Ki.add(i, BigInteger.probablePrime(NumberBits, rnd));
                        Mi.add(i, M.divide(UsersKeys.get(i)));
                        MiInv.add(i, Mi.get(i).modInverse(UsersKeys.get(i)));
                        X = X.add(Ki.get(i).multiply(Mi.get(i)).multiply(MiInv.get(i))).mod(M);
                    } else if (i < Users.size()) {
                        X = X.subtract(Ki.get(i).multiply(Mi.get(i)).multiply(MiInv.get(i)));
                        Ki.set(i, ServerGroupKey.xor(Users.get(i).PrivateKey));
                        X = X.add(Ki.get(i).multiply(Mi.get(i)).multiply(MiInv.get(i))).mod(M);
                    }

                } catch (Exception ex) {
                    System.out.printf(Locale.getDefault(), "i:%d, Last Users:%d, Last Keys:%d,Users.size():%d, UsersKeys.size():%d, KeyPool.size():%d ", i, lastUsers, lastKeys, Users.size(), UsersKeys.size(), KeyPool.size());
                }
            }
        }
        lastUsers = Users.size();
        lastKeys = UsersKeys.size();
        return X.mod(M);
    }


    private static void broadcastX(BigInteger X) {
        for (User u : Users)
            u.computeGroupKey(X);

    }

    private static void memberJoin(int n) {
        long startTime = System.nanoTime();
        if (Users.size() + n >= UsersKeys.size())
            groupInitialization(Users.size() + n, (Users.size() + n) * Factor);
        // keyPoolGeneration((KeyPool.size()+n) * Factor, true);

        for (int i = 0; i < n; i++) {
            try {
                Users.add(new User(UsersKeys.get(Users.size())));
            } catch (Exception ex) {
                System.out.println("ex = " + ex.getMessage());
            }

        }
        X = computeX();
        broadcastX(X);

//        memberJointime = System.nanoTime() - startTime;
    }

    private static LinkedList<User> RemovedUsers = new LinkedList<>();

    private static void memberLeave(int Userid) {
        if (Userid < 0 || Userid >= UsersKeys.size())
            throw new IllegalArgumentException();

        long startTime = System.nanoTime();
        BigInteger removed = UsersKeys.remove(Userid);
        RemovedUsers.add(Users.remove(Userid));

        removeKeyFromPool(removed);
        X = computeX();
        broadcastX(X);
//        memberLeavetime = System.nanoTime() - startTime;

    }

    private static void memberLeave(ArrayList<Integer> Userids) {
        if (Userids == null || Userids.size() < 1)
            throw new IllegalArgumentException();

        // sort the users Ids decadently to grantee
        // that removing an element will not effect the id of the next id to be removed
        Userids.sort((a, b) -> b.compareTo(a));
        long startTime = System.nanoTime();
        BigInteger removed;
        //  int shift =0,index ;
        try {
            for (int id : Userids) {
                removed = UsersKeys.remove(id);
                RemovedUsers.add(Users.remove(id));
                removeKeyFromPool(removed);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        X = computeX();
        broadcastX(X);
        MassLeavetime = System.nanoTime() - startTime;

    }

    private static void removeKeyFromPool(BigInteger removed) {
        BigInteger temp;
        do {
            temp = BigInteger.probablePrime(NumberBits, rnd);
        } while (KeyPool.contains(temp));
        KeyPool.add(temp);
        KeyPool.remove(removed);

    }

    static class User {
        private static int _id = 1;
        private int ID;
        private BigInteger GroupKey, PrivateKey;


        public User(BigInteger Uk) {
            ID = _id++;
            setPrivateKey(Uk);
        }

        public BigInteger getGroupKey() {
            return GroupKey;
        }

        public void setPrivateKey(BigInteger privateKey) {
            PrivateKey = privateKey;
        }

        private void computeGroupKey(BigInteger X) {
            long startTime = System.nanoTime();
            GroupKey = X.mod(PrivateKey).xor(PrivateKey);
            ClientsKeyGetTime += System.nanoTime() - startTime;


        }

        @Override
        public String toString() {
            return "User{" +
                    "ID=" + ID +
                    ", GroupKey=" + GroupKey +
                    ", PrivateKey=" + PrivateKey +
                    '}';
        }
    }
}