package done;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;
import java.util.ArrayList;

import static helper.TimeHelper.timeToString;

/**
 *
 */
public class AMOUN {

    private static final int UsersFactor = 2;
    private static PrintWriter writer = null;
    private static long keyPoolGenerationTime;
    private static long memberJoinTime;
    private static long memberLeaveningTime;
    private static long MassLeavetime;
    private static long startTime;
    private static long InitializationTime = 0;
    private static long ClientsKeyGetTime = 0;


    static {
        try {
            String filename = new Object() {
            }.getClass().getEnclosingClass().getSimpleName();
            writer = new PrintWriter(new FileWriter(new File(filename + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final int MAX_KEY_LENGTH = 1024;
    private static int NumberBits = 32;
    private static final int d = NumberBits + 2;
    private static final int RUNS = 3;   // number of runs to make AVG
    private static final int INITITAL_USERS = 10;  // number of receivers

    private static final Random rnd = new Random();

    private static BigInteger M = BigInteger.ONE;
    private static BigInteger X = BigInteger.ZERO;


    static int[] usersToAdd = {1, 3, 7, 13, 23, 33};

    public static void main(String[] args) {

        boolean result;
        ArrayList<Integer> toRemove = new ArrayList<>();
        writer.printf(Locale.getDefault(), "Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");

        while (NumberBits <= MAX_KEY_LENGTH) {


            // keyPoolGeneration(INITITAL_USERS * UsersFactor);
            groupInitialization(INITITAL_USERS);
            writer.printf(Locale.getDefault(), "%d,%d,%s,%s,%s%n",
                    NumberBits, INITITAL_USERS, timeToString(keyPoolGenerationTime), timeToString(InitializationTime), timeToString(ClientsKeyGetTime / INITITAL_USERS));
            test();
            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            clean();
            NumberBits <<= 1;


        }
        writer.close();


        System.out.println("Done");
//        for(int i=0;i<failureCounters.length;i++)
//        {
//            System.out.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//            writer.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//        }
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void clean() {


        System.gc();
        UsersKeys.clear();
        Users.clear();
        ServerGroupKey = BigInteger.ZERO;
        X = BigInteger.ZERO;
    }

    private static void test() {
        String result = "";
        for (User u : Users) {
            if (ServerGroupKey.compareTo(u.GroupKey) == 0)
                result = "PASSED";
            else
                result = "FAILED!!";
            System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, result, "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
            System.out.println("--------------------------------");
        }
    }

    private static ArrayList<BigInteger> KeyPool;
    private static ArrayList<BigInteger> UsersKeys = new ArrayList<>();
    private static LinkedList<User> Users = new LinkedList<>();

    private static void keyPoolGeneration(int poolSize) {
        BigInteger prime;
        if (KeyPool == null)
            KeyPool = new ArrayList<>(poolSize);
        BigInteger K = BigInteger.ZERO;
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(NumberBits, rnd);
            if (prime.isProbablePrime(10) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }
        keyPoolGenerationTime = System.nanoTime() - startTime;
    }

    private static BigInteger ServerGroupKey = BigInteger.TEN;

    public static BigInteger _N_Temp = BigInteger.ZERO;
    public static BigInteger _Prim_64 = new BigInteger("18446744073709551557");
    public static int _minRange = 22222;
    public static int _maxRange = 65536;
    public static int _countFF = 0;
    public static int ii = 0;

    public static int _NumRun = 0;
    public static int _n = 10;
    public static BigInteger _X = BigInteger.ONE;
    public static ArrayList<BigInteger> _k = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _p = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _q = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _y = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _y_inv = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _N = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _e = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _a = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _d = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _t = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _f = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _m = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _m_decrypt = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _N_p = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _e_p = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _A_EEA = new ArrayList<BigInteger>();
    public static ArrayList<BigInteger> _S = new ArrayList<BigInteger>();
    public static BigInteger _S_all = BigInteger.ZERO;

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static void groupInitialization(int numberOfUsers) {

        generateUsers(numberOfUsers);

        _f = new ArrayList<BigInteger>();
        _e_p = new ArrayList<BigInteger>();
        _A_EEA = new ArrayList<BigInteger>();
        _S = new ArrayList<BigInteger>();

        _N_p = new ArrayList<BigInteger>();
        _N_p.add(BigInteger.probablePrime(NumberBits << 1, rnd));
        _t = new ArrayList<BigInteger>();

        BigInteger _e_final_temp = BigInteger.ZERO;

        BigInteger _f_GCD = BigInteger.ZERO;
        BigInteger _X_send = BigInteger.ONE;
        BigInteger _temp_test = BigInteger.ONE;
        _X = BigInteger.ONE;

        long startTime = System.nanoTime();

        coPrimeTest();
        int i = 0 ;
       while(i < Users.size()) {
//        for (int i = 0; i < _n; i++) {
            for (int f = 100; f < 2000000; f++) {
                _e_final_temp = Users.get(i).E.add(((Users.get(i).D.subtract(BigInteger.ONE)).multiply(BigInteger.valueOf(f))).mod(_N_p.get(i)));       // (9)
                if (_e_final_temp.gcd(_N_p.get(i)).compareTo(BigInteger.ONE) != 0) {
                    _e_p.add(_e_final_temp);
                    break;
                }
            }
            _X = _X.multiply(_N_p.get(i++));              // (10)
        }

        for ( i = 0; i < _n; i++) {
            try {
                _X_send = _X.divide(_N_p.get(i));
                _A_EEA.add(_X_send.modInverse(_N_p.get(i)));
                //  _S.Add(_e_p[i] * _A_EEA[i] * _X_send);
                _S.add(_e_p.get(i).multiply(_A_EEA.get(i)).multiply(_X_send));
                _S_all = _S_all.add(_S.get(i));
            }catch (Exception ed)
            {
                System.out.println(ed.getMessage());
            }
        }

        M = generateGroupKey();
        broadcast(M);

        InitializationTime = System.nanoTime() - startTime;
    }

    private static void broadcast(BigInteger C) {
        for (User user : Users) {
            user.computeGroupKey(C);
        }
    }

    private static void generateUsers(int numberOfUsers) {
        User user;
        while (Users.size() < numberOfUsers) {
            user = new User(NumberBits);
            Users.add(user);
            _N.add(user.N);
            _e.add(user.E);
            _d.add(user.D);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    private static BigInteger generateGroupKey() {
        ServerGroupKey = BigInteger.probablePrime(NumberBits - 2, rnd);
        return (ServerGroupKey.mod(_X).multiply(_S_all.mod(_X)).mod(_X)).mod(_X);
    }

    static BigInteger Ki = BigInteger.TEN, Mi = BigInteger.TEN, MiInv = BigInteger.TEN;


    private static void coPrimeTest() {
       int  _countFF = 0;
        ii = 0;

        BigInteger gcd;
        // 1 <t < n/2
        for (int t = _minRange; t < _maxRange; t++) {
//        while(t = new BigInteger() ){
            _N_Temp = _N.get(ii).add(_d.get(ii).subtract(BigInteger.ONE).multiply(BigInteger.valueOf(t)));      // (8)

            for(BigInteger n : _N_p)
            {
                gcd =  _N_Temp.gcd(n);
                if(! gcd.equals(BigInteger.ONE))
                {
                    _countFF++;
                    break;
                }
            }

            if (_countFF == 0) {
                _N_p.add(_N_Temp);
                t = _minRange;
                ii++;
            } else {
                _countFF = 0;
            }
            if (ii >= Users.size()) {
                break;
            }
        }
        _N_p.remove(0);
    }

    static class User {
        private static int _id = 1;
        private int ID;
        private BigInteger GroupKey;
        private BigInteger N;
        private BigInteger E;
        private BigInteger D;
        private BigInteger K;
        private BigInteger P;
        private BigInteger Q;
        private BigInteger Y;
        private BigInteger YInv;
        private BigInteger A;

        public User(int bits) {
            ID = _id++;
            K = BigInteger.probablePrime(bits, rnd);
            P = BigInteger.probablePrime(bits >> 1, rnd);
            Q = BigInteger.probablePrime(bits >> 1, rnd);
            Y = new BigInteger(bits - 2, rnd);
            YInv = Y.mod(K);
            A = new BigInteger(10, rnd);
            N = K.multiply(P);
            E = (K.multiply(Q).add(YInv)).mod(N);
            D = A.modPow(K.subtract(BigInteger.ONE), N);

        }

        private void computeGroupKey(BigInteger C) {
            startTime = System.nanoTime();
            GroupKey = C.mod(K).multiply(Y).mod(K);
            ClientsKeyGetTime += System.nanoTime() - startTime;
        }
    }
}