package done;

import org.openjdk.jmh.annotations.Benchmark;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;

import static helper.TimeHelper.timeToString;

/**
 * K30  2018 A computationally efficient centralized group key distribution protocol for secure
 */


public class K30CGKD {

    private static final int FACTOR = 4;
    private static PrintWriter writer = null;

    private static ArrayList<BigInteger> KeyPool;
    private static BigInteger ServerGroupKey = BigInteger.ZERO;
    private static BigInteger K;
    private static BigInteger E;

    private static BigInteger X = BigInteger.ONE;
    private static BigInteger M = BigInteger.ONE;
    private static BigInteger MinQi = BigInteger.ZERO;
    private static BigInteger MinXi = BigInteger.ZERO;
    private static BigInteger Delta = BigInteger.ZERO;

    private static ArrayList<BigInteger> Pi = new ArrayList<>();
    private static ArrayList<BigInteger> Qi = new ArrayList<>();
    private static ArrayList<BigInteger> Xi = new ArrayList<>();
    private static ArrayList<BigInteger> Phi = new ArrayList<>();

    private static LinkedList<BigInteger> Hi = new LinkedList<>();
    private static LinkedList<BigInteger> Ri = new LinkedList<>();
    private static LinkedList<BigInteger> Si = new LinkedList<>();
    private static LinkedList<BigInteger> Yi = new LinkedList<>();
    private static LinkedList<User> Users = new LinkedList<>();


    static {
        try {
            String filename = new Object() {
            }.getClass().getEnclosingClass().getSimpleName();
            writer = new PrintWriter(new FileWriter(new File(filename + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final int MAX_KEY_LENGTH = 1024;
    private static final int nRuns = 10;   // number of runs to make AVG
    private static final int INITITAL_USERS = 10;  // number of receivers

    private static  int NumberBits = 32;
    private static long InitializationTime = 0;
    private static long keyPoolGenerationTime;
    private static long ClientsKeyGetTime;


    private static final Random rnd = new Random();


//    @Benchmark
//    @BenchmarkMode(Mode.AverageTime)
    public static void main(String[] args) {

        writer.printf(Locale.getDefault(), "Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");

        while (NumberBits <= MAX_KEY_LENGTH) {

            keyPoolGeneration(INITITAL_USERS * FACTOR, NumberBits);
//            System.out.println("KeyPool = " + KeyPool);
            initialSetUp(INITITAL_USERS, NumberBits);

            writer.printf(Locale.getDefault(), "%d,%d,%s,%s,%s%n",
                    NumberBits, INITITAL_USERS, timeToString(keyPoolGenerationTime), timeToString(InitializationTime), timeToString(ClientsKeyGetTime / INITITAL_USERS));
            // test();
            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            clean();
            NumberBits <<= 1;


        }
        writer.close();


        System.out.println("Done");
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    //
    private static void clean() {
        keyPoolGenerationTime = 0;
//
        InitializationTime = 0;
        ClientsKeyGetTime = 0;
        System.gc();
        Xi.clear();
        Yi.clear();;

        KeyPool.clear();
        M = BigInteger.ONE;
        X = BigInteger.ZERO;

        Users.clear();
        ServerGroupKey=BigInteger.ZERO;
        X=BigInteger.ZERO;
        K = BigInteger.ZERO;
        X = BigInteger.ONE;
        M = BigInteger.ONE;
        MinQi = BigInteger.ZERO;
        MinXi = BigInteger.ZERO;
        Delta = BigInteger.ZERO;
        Pi.clear();
        Qi.clear();
        Phi.clear();
        Hi.clear();
        Ri.clear();
        Si.clear();
        Yi.clear();

    }

    //
    private static void test() {
        String result = "";
        for (User u : Users) {
            if (ServerGroupKey.compareTo(u.getGroupKey()) == 0)
                result = "PASSED";
            else
                result = "FAILED!!";
            System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, result, "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
            System.out.println("--------------------------------");
        }
    }

    //
//
    private static void initialSetUp(int n, int numberBits) {

        long start = System.nanoTime();


        // Compute Qi and Phi
        for (int i = 0; i < n; i++) {
            Pi.add(KeyPool.remove(0));
            Qi.add(KeyPool.remove(0));
            Xi.add(Pi.get(i).multiply(Qi.get(i)));
            Phi.add(Pi.get(i).subtract(BigInteger.ONE).multiply(Qi.get(i).subtract(BigInteger.ONE)));


            Hi.add(KeyPool.remove(0));
        }

        // compute M
        for (int i = 0; i < n; i++)
            M = M.multiply(Xi.get(i));

        // compute Ri

        for (int i = 0; i < n; i++) {
            Ri.add(M.divide(Xi.get(i)));
            // compute the Si, the multiplicative inverse of Ri
            Si.add(Ri.get(i).modInverse(Xi.get(i)));
            // compute yi
            Yi.add(Hi.get(i).multiply(Ri.get(i)).multiply(Si.get(i)));
            // find the min of Qi
            if (Qi.get(i).compareTo(MinQi) == -1)
                MinQi = Qi.get(i);
            // find the min of Xi
            if (Xi.get(i).compareTo(MinXi) == -1)
                MinXi = Xi.get(i);

        }

        // selecting e
        int nLength = Integer.bitCount(n) + 1;
        int MinQiLength = MinQi.bitLength();
        int bits = nLength + rnd.nextInt(MinQiLength - nLength);
        do {
            E = new BigInteger(bits, rnd);
        } while (!E.gcd(MinQi).equals(BigInteger.ONE));


        InitializationTime = System.nanoTime() - start;
        initialJoin(n);
        sharePublickey(E);

        K = new BigInteger(1 + rnd.nextInt(MinXi.bitCount() - 1), rnd);

        BigInteger cipher = encrypt(K);
        broadcast(cipher);


    }

    private static void broadcast(BigInteger cipher) {
        for (User u : Users) {
            u.computeGroupKey(cipher);
        }
    }

    private static BigInteger encrypt(BigInteger k) {
        return Delta.mod(X).multiply(k.modPow(E, X));
    }

    private static void sharePublickey(BigInteger e) {
        for (User u : Users) {
            u.receiveE(e);
        }
    }

    private static void initialJoin(int n) {
        for (int i = 0; i < n; i++) {
            Users.add(new User(Xi.get(i), Phi.get(i)));
            X = X.multiply(Xi.get(i));
            Delta = Delta.add(Yi.get(i));
        }

    }

    private static void keyPoolGeneration(int poolSize, int bit) {
        BigInteger prime;

        if (KeyPool == null)
            KeyPool = new ArrayList<>(poolSize);
        BigInteger K = BigInteger.ZERO;
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(bit, rnd);
            if (prime.isProbablePrime(10) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }
        keyPoolGenerationTime = System.nanoTime() - startTime;
    }


    private static void computingXiAndPhi(int n) {
        for (int i = 0; i < n; i++) {
            Pi.add(KeyPool.remove(0));
            Pi.add(KeyPool.remove(0));
            Xi.add(Pi.get(i).multiply(Qi.get(i)));
            Phi.add(Pi.get(i).subtract(BigInteger.ONE).multiply(Qi.get(i).subtract(BigInteger.ONE)));

            Hi.add(KeyPool.remove(0));
        }
    }

    static class User {
        private static int _id = 0;
        private int ID;
        private BigInteger X, Phi, GroupKey;

        private BigInteger PrivateKey, L;


        public User(BigInteger x, BigInteger phi) {
            ID = _id++;
            X = x;
            Phi = phi;

        }

        public BigInteger getGroupKey() {
            return GroupKey;
        }


        private void computeGroupKey(BigInteger cipher) {
            long startTime = System.nanoTime();

            GroupKey = cipher.mod(X).multiply(L).mod(X).modPow(PrivateKey, X);
            ClientsKeyGetTime += System.nanoTime() - startTime;

        }

        public void receiveE(BigInteger e) {
            // TODO: compute Di using extended Eculidean
            PrivateKey = e.modInverse(Phi);
            // where Hi come from to the member
            L = Hi.get(ID).modInverse(X);
        }


    }
}