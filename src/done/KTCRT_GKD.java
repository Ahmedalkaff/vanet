package done;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;
import static helper.TimeHelper.timeToString;

/**
 * K21 From K12 2009  Key tree and Chinese remainder theorem based group key distribution scheme
 */
public class KTCRT_GKD {

    /// User Tree related variables
    private static final int D = 2;
    private static int l = 2;
    private static int n = 2;


    /// Time variable

    private static long keyPoolGenerationTime;
    private static long groupInitializationTime = 0;
    private static long ClientsKeyGetTime ;

    /// Report related variables
    private static PrintWriter writer = null;

    static {
        try {
            String filename = new Object(){}.getClass().getEnclosingClass().getSimpleName();
            writer = new PrintWriter(new FileWriter(new File(filename+".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /// Bits related variable

    private static final int MAX_KEY_LENGTH = 1024;
    private static int NumberBits = 16;


    /// users related variables
    private static final int INITIAL_USERS = 100;  // number of receivers
    private static final int FACTOR = 2;


    /// Other variables
    private static final Random rnd = new Random();

    /// Algorithm related variables
    private static ArrayList<BigInteger> KeyPool;
    private static ArrayList<BigInteger> UsersKeys = new ArrayList<>();
    private static Map<Integer, User> Users = new HashMap<>();
    private static ArrayList<BigInteger> Ui = new ArrayList<>();
    private static Tree<BigInteger> GroupTree = new Tree<>();

    private static BigInteger M = BigInteger.ONE;
    private static BigInteger X = BigInteger.ZERO;
    private static BigInteger ServerGroupKey = BigInteger.TEN;

    public static void main(String[] args) {

        boolean result;
        ArrayList<Integer> toRemove = new ArrayList<>();
        writer.printf(Locale.getDefault(), "Number of bits,Initial Users,keyPoolGeneration time,group Initialization time,average key get time%n");

        while (NumberBits <= MAX_KEY_LENGTH) {

            l = (int) Math.ceil(Math.log(INITIAL_USERS) / Math.log(D));
            n = (int) Math.pow(D, l + 1) - 1;

            KeyPool = keyPoolGeneration(n*FACTOR);
//            System.out.println("KeyPool = " + KeyPool);
            groupInitialization(INITIAL_USERS);
            writer.printf(Locale.getDefault(), "%d,%d,%s,%s,%s%n",
                    NumberBits, INITIAL_USERS, timeToString(keyPoolGenerationTime), timeToString(groupInitializationTime), timeToString(ClientsKeyGetTime / INITIAL_USERS));
           // test();
            writer.flush();
            System.out.println("NumberBits = " + NumberBits);
            clean();
            NumberBits <<= 1;


        }
        writer.close();

        System.out.println("Done");
//        for(int i=0;i<failureCounters.length;i++)
//        {
//            System.out.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//            writer.printf(Locale.getDefault(),"Round [%d] :%d/%d%n",i,failureCounters[i],RUNS);
//        }
        writer.println("\n\n::The End:: \n\n");
        writer.close();
    }

    private static void test() {
        String result = "";
        for (User u : Users.values()) {
            if (ServerGroupKey.compareTo(u.GroupKey) == 0)
                result = "PASSED";
            else
                result = "FAILED!!";
            System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, result, "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
            System.out.println("--------------------------------");
        }
    }

    private static void clean() {

        System.gc();
        X = BigInteger.ZERO;
        M = BigInteger.ONE;
        ServerGroupKey = null;

        KeyPool.clear();
        UsersKeys.clear();
        Users.clear();
        Ui.clear();
    }

//    private static void test() {
//        for (User u : Users) {
//            if (ServerGroupKey.compareTo(u.GroupKey) == 0) {
//                System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n", u.ID, NumberBits, "PASSED");
//                System.out.println("--------------------------------");
//            } else {
//                System.out.printf(Locale.getDefault(), "%-3d , %-8d ,%20s %n%15s%s%n%15s%s%n", u.ID, NumberBits, "FAILED", "User GK:", u.GroupKey, "ServerGroupKey:", ServerGroupKey);
//                System.out.println("--------------------------------");
//            }
//
//
//        }
//    }



    private static ArrayList<BigInteger> keyPoolGeneration(int poolSize) {
        BigInteger prime;

        if (KeyPool == null )
            KeyPool = new ArrayList<>(poolSize);
        long startTime = System.nanoTime();
        while (KeyPool.size() < poolSize) {
            prime = BigInteger.probablePrime(NumberBits, rnd);
            if (prime.isProbablePrime(3) && !KeyPool.contains(prime)) {
                KeyPool.add(prime);
            }
        }

        keyPoolGenerationTime = System.nanoTime() - startTime;
        return KeyPool ;
    }

    private static void groupInitialization(int numberOFUsers) {
        if (numberOFUsers < 0 || numberOFUsers >= KeyPool.size())
            throw new IllegalArgumentException();

        long startTime = System.nanoTime();

        GroupTree = generateTree(numberOFUsers);

//        System.out.println("GroupTree = " + GroupTree);
        Users = generateUserSet(GroupTree, numberOFUsers);

//        System.out.println("Users = " + Users);
//        System.out.println("Users.keySet() = " + Users.keySet());
        sendPrivatekeySet(GroupTree, Users);

        LinkedList<Integer> node = new LinkedList<>(Users.keySet());
        LinkedList<Integer> forst = new LinkedList<>();
        GroupTree.RootID(forst, node);
        if (forst.size() == 1)

            ServerGroupKey = GroupTree.getKey(0);

        else {
            do
                ServerGroupKey = generateGroupKey();
            while (!isValidGroupKey(Users, ServerGroupKey));

            Ui.clear();
            M = BigInteger.ONE;
            for (Integer id : forst) {
                Ui.add(ServerGroupKey.xor(GroupTree.getKey(id)));
                M = M.multiply(GroupTree.getKey(id));
            }
//            System.out.println("ServerGroupKey = " + ServerGroupKey);
            X = computeX(GroupTree, M, Ui, forst);

        }

        broadcastX(forst, X);
        groupInitializationTime = System.nanoTime() - startTime;
    }

    private static boolean isValidGroupKey(Map<Integer, User> users, BigInteger groupKey) {
        for (User user : users.values()) {
            if (groupKey.compareTo(user.PrivateKey) == 0)
                return false;
        }
        return true;
    }

    private static void sendPrivatekeySet(Tree<BigInteger> tree, Map<Integer, User> users) {

        for (Integer id : users.keySet()) {
            users.get(id).KeySet = tree.getKeySet(id);
        }
    }


    private static Tree<BigInteger> generateTree(int users) {

        l = (int) Math.ceil(Math.log(users) / Math.log(D));
        n = (int) Math.pow(D, l + 1) - 1;

        if (n >= KeyPool.size())
            throw new IllegalArgumentException();


        Tree<BigInteger> tree = new Tree<>(D);
        for (int i = 0; i < n; i++) {
            tree.add(i, KeyPool.remove(0), (i - 1) / D);
        }

        return tree;

    }


    private static Map<Integer, User> generateUserSet(Tree<BigInteger> tree, int numUsers) {

        if (tree.getLeavesNumber() < numUsers)
            throw new IllegalArgumentException();

        User user;
        int firstleaveId = tree.firstLeaveID(), leaves = tree.getLeavesNumber();
        int id;
        Random random = new Random();

        boolean useRandom = numUsers < tree.getLeavesNumber();
        Map<Integer, User> users = new HashMap<>();
        id = firstleaveId - 1;
        while (users.size() < numUsers) {
            if (useRandom) {
                do
                    id = firstleaveId + random.nextInt(leaves);
                while (users.containsKey(id));
            } else
                id++;

            user = new User(id, tree.getKey(id));
            users.put(id, user);

        }

        return users;
    }


    private static BigInteger generateGroupKey() {
        return BigInteger.probablePrime(NumberBits - 1, rnd);
    }

    static ArrayList<BigInteger> Ki = new ArrayList<>();
    static ArrayList<BigInteger> Mi = new ArrayList<>();
    static ArrayList<BigInteger> MiInv = new ArrayList<>();

    static int lastUsers = 0, lastKeys = 0;

    private static BigInteger computeX(Tree<BigInteger> T, BigInteger M, ArrayList<BigInteger> Ui, LinkedList<Integer> forst) {

        BigInteger X = BigInteger.ZERO;
//        System.out.println("M = " + M);
        BigInteger Mi, Minv;
        if (Ui.size() != forst.size())
            throw new IllegalArgumentException();

//        System.out.println("Ui = " + Ui);
        for (int idx = 0; idx < forst.size(); idx++) {
            Mi = M.divide(T.getKey(forst.get(idx)));
            Minv = Mi.modInverse(T.getKey(forst.get(idx)));
            X = X.add((Ui.get(idx).multiply(Mi).multiply(Minv)).mod(M));
        }
        return X.mod(M);
    }


    private static void broadcastX(LinkedList<Integer> forst, BigInteger X) {
        for (User u : Users.values()) {
            try {
                u.computeGroupKey(forst, X);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    static class User {
        private static int _id = 1;
        private int ID;
        private BigInteger GroupKey, PrivateKey;
        private Map<Integer, BigInteger> KeySet;
        private long startTime;

        public User(int id, BigInteger Uk) {
            ID = id;
            setPrivateKey(Uk);
        }

        public BigInteger getGroupKey() {
            return GroupKey;
        }

        public void setPrivateKey(BigInteger privateKey) {
            PrivateKey = privateKey;
        }

        private void computeGroupKey(LinkedList<Integer> forst, BigInteger X) throws Exception {
            long startTime = System.nanoTime();
            BigInteger K = null;
            for (Integer j : forst) {
                if (KeySet.keySet().contains(j)) {
                    K = KeySet.get(j);

                    break;
                }
            }
            if (K == null)
                throw new Exception("User is eliminated");

//
//            System.out.println("ID = " + ID);
//            System.out.println("K = " + K);
//            System.out.println("X = " + X);
//            System.out.println("forst = " + forst);
//            System.out.println("KeySet = " + KeySet);
//            System.out.println("PrivateKey = " + PrivateKey);;
            GroupKey = (X.mod(K)).xor(K);
            ClientsKeyGetTime += System.nanoTime() - startTime;

        }

        @Override
        public String toString() {
            return "User{" +
                    "ID=" + ID +
                    ", GroupKey=" + GroupKey +
                    ", PrivateKey=" + PrivateKey +
                    '}';
        }
    }

    public static  class Tree<T> {

        int d = 2;
        int l = 0 ;
        private LinkedList<Tree.TreeNode<T>> tree;

        public Tree(int d) {
            this.tree = new LinkedList<Tree.TreeNode<T>>();
            this.d = d;
        }

        public Tree() {
            this(2);
        }

        public void add(int id, T t, Integer parentId) {
            if (id < 0 || parentId < 0 || (id > 0 && parentId == null))
                throw new IllegalArgumentException();

            Tree.TreeNode<T> tn;
            if (id == 0)
                tn = new Tree.TreeNode<>(tree.size(), null, t);
            else
                tn = new Tree.TreeNode<>(tree.size(), parentId, t);

            tree.add(tn);
        }

        public int getTreeLength() {return  (int)  (Math.log(tree.size())/Math.log(d));}
        public int getLeavesNumber() {return (int) Math.pow(d,getTreeLength());}

        public Tree.TreeNode<T> getParentNode(int id) {
            if (id < 0 || id >= tree.size())
                throw new IllegalArgumentException();


            Integer parentID =tree.get(id).ParentId ;
            if(parentID == null)
                return  null;

            return tree.get(parentID);
        }

        public T getKey(int id)
        {
            if (id < 0 || id >= tree.size())
                throw new IllegalArgumentException();
            return tree.get(id).Data ;
        }

//    public Map<Integer,T> getPrivateKeySet(Integer id) {
//        if (id < 0 || id >= tree.size())
//            throw new IllegalArgumentException();
//        HashMap<Integer,T> map = new HashMap<>();
//        do{
//            map.put(id,tree.get(id).Data);
//            id = getParentNode(id).Id;
//        }
//        while ( id != null);
//
//        return map;
//    }


        @Override
        public String toString() {
            return tree.toString();
        }


        public void RootID(LinkedList<Integer> forest, LinkedList<Integer> node) {

            boolean AllEqual = false;
            int index = 0;
            while (node.size() >= d) {

                Integer[] parentIDs = new Integer[d];
                for (int i = 0; i < parentIDs.length; i++) {
                    try {
                        parentIDs[i] = getParentNode(node.get(i)).Id;
                        if (i == 0)
                            AllEqual = true;
                        else
                            AllEqual &= parentIDs[i].equals(parentIDs[i - 1]);
                    } catch (Exception ex) {
                        System.out.println("ex = " + ex);
                    }

                }
                if (AllEqual) {
                    node.add(parentIDs[0]);
                    for (int i = 0; i < d; i++)
                        node.removeFirst();
                } else {
                    forest.add(node.removeFirst());
                }

            }

            while (node.size() > 0)
                forest.add(node.removeFirst());
        }

        public Map<Integer, T> getKeySet(Integer id) {
            HashMap<Integer, T> keySet = new HashMap<>();
            if (id < tree.size() / d)
                throw new IndexOutOfBoundsException("ID is not for a leaf node:ID:" + id);
            Tree.TreeNode<T> parent ;
            while (id != null) {
                keySet.put(id, tree.get(id).Data);
                parent = getParentNode(id);
                if(parent != null)
                    id = parent.Id;
                else
                    break;
            }

            return keySet;
        }

        public int size() {
            return tree.size();
        }

        public int firstLeaveID() {
            return (tree.size()-1)/d;
        }

        static class TreeNode<T> {
            private int Id;
            private Integer ParentId;
            private T Data;

            public TreeNode(int id, Integer parentId, T data) {
                Id = id;
                ParentId = parentId;
                Data = data;
            }

            @Override
            public String toString() {
                return "[" +
                        "Id=" + Id +
                        ", ParentId=" + ParentId +
                        ", Data=" + Data +
                        "]\n";
            }
        }


    }

}