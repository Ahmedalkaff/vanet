package KTCRT;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Tree<T> {

    int d = 2;
    int l = 0 ;
    private LinkedList<TreeNode<T>> tree;

    public Tree(int d) {
        this.tree = new LinkedList<TreeNode<T>>();
        this.d = d;
    }

    public Tree() {
        this(2);
    }

    public void add(int id, T t, Integer parentId) {
        if (id < 0 || parentId < 0 || (id > 0 && parentId == null))
            throw new IllegalArgumentException();

        TreeNode<T> tn;
        if (id == 0)
            tn = new TreeNode<>(tree.size(), null, t);
        else
            tn = new TreeNode<>(tree.size(), parentId, t);

        tree.add(tn);
    }

    public int getTreeLength() {return  (int)  (Math.log(tree.size())/Math.log(d));}
    public int getLeavesNumber() {return (int) Math.pow(d,getTreeLength());}

    public TreeNode<T> getParentNode(int id) {
        if (id < 0 || id >= tree.size())
            throw new IllegalArgumentException();


        Integer parentID =tree.get(id).ParentId ;
        if(parentID == null)
            return  null;

        return tree.get(parentID);
    }

    public T getKey(int id)
    {
        if (id < 0 || id >= tree.size())
            throw new IllegalArgumentException();
        return tree.get(id).Data ;
    }

//    public Map<Integer,T> getPrivateKeySet(Integer id) {
//        if (id < 0 || id >= tree.size())
//            throw new IllegalArgumentException();
//        HashMap<Integer,T> map = new HashMap<>();
//        do{
//            map.put(id,tree.get(id).Data);
//            id = getParentNode(id).Id;
//        }
//        while ( id != null);
//
//        return map;
//    }


    @Override
    public String toString() {
        return tree.toString();
    }


    public void RootID(LinkedList<Integer> forest, LinkedList<Integer> node) {

        boolean AllEqual = false;
        int index = 0;
        while (node.size() >= d) {

            Integer[] parentIDs = new Integer[d];
            for (int i = 0; i < parentIDs.length; i++) {
                try {
                    parentIDs[i] = getParentNode(node.get(i)).Id;
                    if (i == 0)
                        AllEqual = true;
                    else
                        AllEqual &= parentIDs[i].equals(parentIDs[i - 1]);
                } catch (Exception ex) {
                    System.out.println("ex = " + ex);
                }

            }
            if (AllEqual) {
                node.add(parentIDs[0]);
                for (int i = 0; i < d; i++)
                    node.removeFirst();
            } else {
                forest.add(node.removeFirst());
            }

        }

        while (node.size() > 0)
            forest.add(node.removeFirst());
    }

    public Map<Integer, T> getKeySet(Integer id) {
        HashMap<Integer, T> keySet = new HashMap<>();
        if (id < tree.size() / d)
            throw new IndexOutOfBoundsException("ID is not for a leaf node:ID:" + id);
        TreeNode<T> parent ;
        while (id != null) {
            keySet.put(id, tree.get(id).Data);
            parent = getParentNode(id);
            if(parent != null)
                id = parent.Id;
            else
                break;
        }

        return keySet;
    }

    public int size() {
        return tree.size();
    }

    public int firstLeaveID() {
        return (tree.size()-1)/d;
    }

    static class TreeNode<T> {
        private int Id;
        private Integer ParentId;
        private T Data;

        public TreeNode(int id, Integer parentId, T data) {
            Id = id;
            ParentId = parentId;
            Data = data;
        }

        @Override
        public String toString() {
            return "[" +
                    "Id=" + Id +
                    ", ParentId=" + ParentId +
                    ", Data=" + Data +
                    "]\n";
        }
    }


}
