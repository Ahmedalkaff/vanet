using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Diagnostics;
using System.IO;

namespace AMOUN_Code_2018
{
    class Program
    {
        static void Main(string[] args)
        {
            done.AMOUN();
            Console.ReadLine();
        }

        private static void done.AMOUN()
        {
            _NumRun = 1;
            _n = 10;

            _StName += "done.AMOUN -- Key 32-bit -- MessNum" + _n + " -- Run " + _NumRun + " -- " + DateTimeOffset.Now.ToString().Replace("/", "-").Replace(":", ".") + ".txt";
            File.WriteAllText(_StName, string.Empty);

            KeyGeneration();
            for (int i = 0; i < _NumRun; i++)
            {
                Initialization();
                Encryption();
                Decryption();
            }

            Console.WriteLine("\n\n::The End:: \n\n");

            StoreFinalValues();
        }

        private static void StoreFinalValues()
        {
            _FinalOutPut = "===========================================================================" + Environment.NewLine;
            _FinalOutPut += ":: Total Initialization Time:  " + _InitiaTime + Environment.NewLine;
            _InitiaTimeAVG = _InitiaTime / _NumRun;
            _FinalOutPut += ":: AVG Initialization Time:    " + _InitiaTimeAVG + Environment.NewLine + Environment.NewLine;

            _FinalOutPut += ":: Total Encryption Time:      " + _EncTime + Environment.NewLine;
            _EncTimeAVG = _EncTime / _NumRun;
            _FinalOutPut += ":: AVG Encryption Time:        " + _EncTimeAVG + Environment.NewLine + Environment.NewLine;

            _FinalOutPut += ":: Total Decryption Time:      " + _DecTime + Environment.NewLine;
            _DecTimeAVG = _DecTime / _NumRun;
            _FinalOutPut += ":: AVG Decryption Time:        " + _DecTimeAVG + Environment.NewLine;
            _FinalOutPut += "===========================================================================" + Environment.NewLine;
            Console.WriteLine("NumRun {0}    InitiaTime     {1}  InitiaTimeAVG     {2}", _NumRun, _InitiaTime, _InitiaTimeAVG);
            Console.WriteLine("NumRun {0}    EncTime        {1}  EncTimeAVG        {2}", _NumRun, _EncTime, _EncTimeAVG);
            Console.WriteLine("NumRun {0}    DecTime        {1}  DecTimeAVG        {2}", _NumRun, _DecTime, _DecTimeAVG);
            write(_StName, _FinalOutPut);
        }

        private static void write(string _FileName, string _Data)
        {
            using (StreamWriter sw = File.AppendText(_FileName))
            {
                sw.WriteLine(_Data);
            }
        }

        public static string _StName = "";
        public static string _FinalOutPut = "";

        public static Stopwatch _Timer1 = new Stopwatch();

        public static long _InitiaTime = 0;
        public static long _InitiaTimeAVG = 0;
        public static long _EncTime = 0;
        public static long _EncTimeAVG = 0;
        public static long _DecTime = 0;
        public static long _DecTimeAVG = 0;



        public static int _NumRun = 0;
        public static int _n = 0;
        public static BigInteger _X = 1;
        public static List<BigInteger> _k = new List<BigInteger>();
        public static List<BigInteger> _p = new List<BigInteger>();
        public static List<BigInteger> _q = new List<BigInteger>();
        public static List<BigInteger> _y = new List<BigInteger>();
        public static List<BigInteger> _y_inv = new List<BigInteger>();
        public static List<BigInteger> _N = new List<BigInteger>();
        public static List<BigInteger> _e = new List<BigInteger>();
        public static List<BigInteger> _a = new List<BigInteger>();
        public static List<BigInteger> _d = new List<BigInteger>();
        public static List<BigInteger> _t = new List<BigInteger>();
        public static List<BigInteger> _f = new List<BigInteger>();
        public static List<BigInteger> _m = new List<BigInteger>();
        public static List<BigInteger> _m_decrypt = new List<BigInteger>();
        public static List<BigInteger> _N_p = new List<BigInteger>();
        public static List<BigInteger> _e_p = new List<BigInteger>();
        public static List<BigInteger> _A_EEA = new List<BigInteger>();
        public static List<BigInteger> _S = new List<BigInteger>();
        public static BigInteger _S_all = 0;


        // Returns modulo inverse of a with
        // respect to m using extended Euclid
        // Algorithm Assumption: a and m are
        // coprimes, i.e., gcd(a, m) = 1
        static BigInteger modInverse(BigInteger a, BigInteger m)
        {
            BigInteger m0 = m;
            BigInteger y = 0, x = 1;

            if (m == 1)
                return 0;

            while (a > 1)
            {
                // q is quotient
                BigInteger q = a / m;
                BigInteger t = m;

                // m is remainder now, process
                // same as Euclid's algo
                m = a % m;
                a = t;
                t = y;

                // Update x and y
                y = x - q * y;
                x = t;
            }

            // Make x positive
            if (x < 0)
                x += m0;

            return x;
        }


        private static BigInteger GCD_Test(BigInteger a, BigInteger b)
        {
            if (a == 0)
                return b;
            return GCD_Test(b % a, a);
        }


        public static BigInteger _N_Temp = 0;
        public static BigInteger _Prim_64 = 18446744073709551557;
        public static int _minRange = 22222;
        public static int _maxRange = 65536;
        public static int _countFF = 0;
        public static int ii = 0;


        private static void Coprime_Test()
        {
            _countFF = 0;
            ii = 0;

            for (int t = _minRange; t < _maxRange; t++)
            {
                _N_Temp = _N[ii] + ((_d[ii] - 1) * t);

                for (int y = 0; y < _N_p.Count; y++)
                {
                    if (GCD_Test(_N_Temp, _N_p[y]) != 1)
                    {
                        _countFF++;
                        break;
                    }
                }
                if (_countFF == 0)
                {
                    _N_p.Add(_N_Temp);
                    t = _minRange;
                    ii++;
                }
                else
                {
                    _countFF = 0;
                }
                if (ii >= _n)
                {
                    break;
                }
            }
            _N_p.RemoveAt(0);
        }


        private static void Decryption()
        {
            BigInteger _MM = 0;
            _m_decrypt = new List<BigInteger>();
            _Timer1.Reset();
            _Timer1.Start();

            for (int i = 0; i < _n; i++)
            {
                _m_decrypt.Add((((_C % _k[i]) * _y[i]) % _k[i]));

                Console.WriteLine("M {0}        M_decrypt {1} ", _m[0], _m_decrypt[i]);
            }
            _Timer1.Stop();
            _DecTime += _Timer1.ElapsedTicks;
        }

        public static BigInteger _C = 0;

        private static void Encryption()
        {

            _m.Add(1524200607);
            _m.Add(1553968403);
            _m.Add(1575729728);
            _m.Add(1312199592);
            _m.Add(1043177704);
            _m.Add(1621880827);
            _m.Add(1852717674);
            _m.Add(1747646737);
            _m.Add(1692227643);
            _m.Add(1400258299);

            _C = 0;

            _Timer1.Reset();
            _Timer1.Start();

            _C = (_m[0] * _S_all) % _X;

            _Timer1.Stop();
            _EncTime += _Timer1.ElapsedTicks;
            Console.WriteLine("The ciphertext:  {0} ", _C);
        }

        private static void Initialization()
        {

            _f = new List<BigInteger>();
            _e_p = new List<BigInteger>();
            _A_EEA = new List<BigInteger>();
            _S = new List<BigInteger>();

            _N_p = new List<BigInteger>();
            _N_p.Add(_Prim_64);
            _t = new List<BigInteger>();

            BigInteger _e_final_temp = 0; ;
            BigInteger _f_GCD = 0;
            BigInteger _X_send = 1;
            BigInteger _temp_test = 1;
            _X = 1;

            _Timer1.Reset();
            _Timer1.Start();

            Coprime_Test();

            for (int i = 0; i < _n; i++)
            {
                for (int f = 100; f < 2000000; f++)
                {
                    _e_final_temp = (_e[i] + ((_d[i] - 1) * f)) % _N_p[i];

                    if (GCD_Test(_e_final_temp, _N_p[i]) != 1)
                    {
                        _e_p.Add(_e_final_temp);
                        break;
                    }
                }
                _X *= _N_p[i];
            }

            for (int i = 0; i < _n; i++)
            {
                _X_send = _X / _N_p[i];
                _A_EEA.Add(modInverse(_X_send, _N_p[i]));
                _S.Add(_e_p[i] * _A_EEA[i] * _X_send);
                _S_all += _S[i];
            }

            _Timer1.Stop();
            _InitiaTime += _Timer1.ElapsedTicks;
        }

        private static void KeyGeneration()
        {
            _k.Add(2103476027);
            _k.Add(1919776231);
            _k.Add(2116938521);
            _k.Add(1829224249);
            _k.Add(2052468853);
            _k.Add(2012814857);
            _k.Add(1982014319);
            _k.Add(1833900749);
            _k.Add(1733137229);
            _k.Add(2008622081);

            _p.Add(27941);
            _p.Add(24799);
            _p.Add(31957);
            _p.Add(24239);
            _p.Add(25243);
            _p.Add(29443);
            _p.Add(23159);
            _p.Add(23833);
            _p.Add(25301);
            _p.Add(22259);


            _q.Add(27271);
            _q.Add(24407);
            _q.Add(22511);
            _q.Add(22123);
            _q.Add(22037);
            _q.Add(29917);
            _q.Add(27211);
            _q.Add(23321);
            _q.Add(23209);
            _q.Add(27551);

            _y.Add(1871948233);
            _y.Add(709757887);
            _y.Add(1553708278);
            _y.Add(912428699);
            _y.Add(977577999);
            _y.Add(596542289);
            _y.Add(687483943);
            _y.Add(864386985);
            _y.Add(408285613);
            _y.Add(25880625);

            _y_inv.Add(1661152850);
            _y_inv.Add(826394604);
            _y_inv.Add(497199306);
            _y_inv.Add(1089071491);
            _y_inv.Add(561989896);
            _y_inv.Add(1323509420);
            _y_inv.Add(803906953);
            _y_inv.Add(1406765427);
            _y_inv.Add(1204993787);
            _y_inv.Add(947741638);

            _a.Add(101);
            _a.Add(103);
            _a.Add(107);
            _a.Add(109);
            _a.Add(113);
            _a.Add(127);
            _a.Add(131);
            _a.Add(137);
            _a.Add(139);
            _a.Add(149);

            for (int i = 0; i < _n; i++)
            {
                _N.Add(_k[i] * _p[i]);
                _e.Add((_k[i] * _q[i] + _y_inv[i]) % _N[i]);
                _d.Add(BigInteger.ModPow(_a[i], (_k[i] - 1), _N[i]));
            }
        }
    }
}
