import java.math.BigInteger;
import java.util.Locale;
import java.util.Random;

public class TestPrimes {

    public static void main(String[] args) {
        boolean IsPrime = true;
        int numberOfBits = 256;
        BigInteger prime, root;
        Random rnd = new Random();
        for (int i = 0; i < 1000; i++) {
            IsPrime = true;
            prime = BigInteger.probablePrime(numberOfBits, rnd);


            root = prime.sqrt();
            for (BigInteger b = BigInteger.TWO; b.compareTo(root) == 1; b.add(BigInteger.ONE)) {

                if (prime.mod(b).compareTo(BigInteger.ZERO) == 0) {
                    System.out.printf(Locale.getDefault(), "%d is not a prime%n", prime);
                    IsPrime = false;
                    break;
                }
            }
//            if(IsPrime)
//                System.out.printf(Locale.getDefault(),"%d is a prime%n",prime);

        }
        System.out.println("Done");
    }
}

